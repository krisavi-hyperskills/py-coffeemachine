class Coffee:
    cups = 1
    water = 200
    milk = 50
    beans = 15
    cost = 6

    def __init__(self, water, milk, beans, cost):
        self.water = water
        self.milk = milk
        self.beans = beans
        self.cost = cost


espresso = Coffee(250, 0, 16, 4)
latte = Coffee(350, 75, 20, 7)
cappuccino = Coffee(200, 100, 12, 6)


class CoffeeMachine:
    water = 400
    milk = 540
    beans = 120
    cups = 9
    money = 550
    fill_order = ['water', 'milk', 'beans', 'cups']

    def __init__(self, choice):
        self.machine_state = 'main_menu'
        self.state = choice

    def remaining(self):
        print(f"The coffee machine has: ")
        print(f"{self.water} ml of water")
        print(f"{self.milk} ml of milk")
        print(f"{self.beans} g of coffee beans")
        print(f"{self.cups} of disposable cups")
        print(f"${self.money} of money")

    def fill(self, user_input):
        setattr(self, self.machine_state[5:], getattr(self, self.machine_state[5:]) + int(user_input))

    def take(self):
        print(f"I gave you ${self.money}")
        self.money = 0

    def buy(self, user_input):
        coffee_choice = user_input
        if coffee_choice == 'back':
            return
        elif int(coffee_choice) == 1:
            selection = espresso
        elif int(coffee_choice) == 2:
            selection = latte
        elif int(coffee_choice) == 3:
            selection = cappuccino
        else:
            return

        if self.water - selection.water < 0:
            print("Sorry, not enough water!")
        elif self.milk - selection.milk < 0:
            print("Sorry, not enough milk!")
        elif self.beans - selection.beans < 0:
            print("Sorry, not enough coffee beans!")
        elif self.cups - selection.cups < 0:
            print("Sorry, not enough disposable cups!")
        else:
            print("I have enough resources, making you a coffee!")
            self.water -= selection.water
            self.milk -= selection.milk
            self.beans -= selection.beans
            self.cups -= selection.cups
            self.money += selection.cost

    def set_state(self, choice):
        self.state = choice
        if choice == 'buy':
            self.machine_state = 'buy_menu'
            print("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino: ", end='')
        elif choice == 'fill':
            self.fill_order = ['water', 'milk', 'beans', 'cups']
            self.set_machine_state('fill_' + self.fill_order.pop(0))
            print("Write how many ml of water do you want to add: ", end='')
        elif choice == 'take':
            self.machine_state = 'take_money'
        elif choice == 'remaining':
            self.machine_state = 'machine_status'
        elif choice == 'menu':
            self.machine_state = 'main_menu'
            print("Write action (buy, fill, take, remaining, exit): ", end='')
        elif choice == 'exit':
            self.machine_state = 'exiting'
        else:
            self.set_state('menu')

    def set_machine_state(self, machine_state):
        self.machine_state = machine_state

    def interface(self, user_input: str):
        if self.machine_state == 'main_menu':
            self.set_state(user_input)
        elif self.machine_state == 'buy_menu':
            getattr(self, self.state)(user_input)
            self.set_state('menu')
        elif 'fill' in self.machine_state:
            getattr(self, self.state)(user_input)
            if len(self.fill_order) != 0:
                next_fill = self.fill_order.pop(0)
                self.set_machine_state('fill_' + next_fill)
            else:
                self.set_state('menu')
                return True
            if next_fill == 'milk':
                print("Write how many ml of milk do you want to add: ", end='')
            elif next_fill == 'beans':
                print("Write how many g of coffee beans do you want to add: ", end='')
            elif next_fill == 'cups':
                print("Write how many disposable cups do you want to add: ", end='')
            return True

        if self.machine_state == 'take_money':
            getattr(self, self.state)()
            self.set_state('menu')
        if self.machine_state == 'machine_status':
            getattr(self, self.state)()
            self.set_state('menu')
        if self.machine_state == 'exiting':
            return False
        return True


coffee_machine = CoffeeMachine("")
coffee_machine.set_state('menu')
while coffee_machine.interface(input()):
    pass
